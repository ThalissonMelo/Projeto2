package projeto_oo_2;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Imagem {
    private String numeroMagico;
    private String segundaLinha;
    private int colunas;
    private int linhas;
    private int pixel;
    private byte[] imagemCopia;
    private String tudoJunto;
    private String magicNumber;
    private int initialPosition;
    private byte[] imagemNegativa;

    public byte[] getImagemNegativa() {
        return imagemNegativa;
    }

    public void setImagemNegativa(byte[] imagemNegativa) {
        this.imagemNegativa = imagemNegativa;
    }

    public String getMagicNumber() {
        return magicNumber;
    }

    public void setMagicNumber(String magicNumber) {
        this.magicNumber = magicNumber;
    }

    public int getInitialPosition() {
        return initialPosition;
    }

    public void setInitialPosition(int initialPosition) {
        this.initialPosition = initialPosition;
    }

    public String getTudoJunto() {
        return tudoJunto;
    }

    public void setTudoJunto(String tudoJunto) {
        this.tudoJunto = tudoJunto;
    }

    public byte[] getImagemCopia() {
        return imagemCopia;
    }

    public void setImagemCopia(byte[] imagemCopia) {
        this.imagemCopia = imagemCopia;
    }

    public String getNumeroMagico() {
        return numeroMagico;
    }

    public void setNumeroMagico(String numeroMagico) {
        this.numeroMagico = numeroMagico;
    }

    public String getSegundaLinha() {
        return segundaLinha;
    }

    public void setSegundaLinha(String segundaLinha) {
        this.segundaLinha = segundaLinha;
    }

    public int getColunas() {
        return colunas;
    }

    public void setColunas(int colunas) {
        this.colunas = colunas;
    }

    public int getLinhas() {
        return linhas;
    }

    public void setLinhas(int linhas) {
        this.linhas = linhas;
    }

    public int getPixel() {
        return pixel;
    }

    public void setPixel(int pixel) {
        this.pixel = pixel;
    }
    
    Scanner input = new Scanner(System.in);
    Scanner input2 = new Scanner(System.in);
    String colunas_e_Linhas;
    String pixels;
    
    public Imagem(String nomeDoArquivo) throws FileNotFoundException{     
    
    try{
            int count=0;
            FileInputStream image = new  FileInputStream(nomeDoArquivo);
            FileInputStream imagePixels = new FileInputStream(nomeDoArquivo);
            Scanner reader = new Scanner(image);

            String tempComment = new String();
        
            this.setMagicNumber(reader.next());
            tempComment += reader.next();
            this.setInitialPosition(reader.nextInt());    
        
            
            FileReader arquivo = new FileReader(nomeDoArquivo);
            BufferedReader lerArquivo = new BufferedReader(arquivo);
            setNumeroMagico(lerArquivo.readLine());
            setSegundaLinha(lerArquivo.readLine());
            colunas_e_Linhas = lerArquivo.readLine();
            input = new Scanner(colunas_e_Linhas);
            input2 = new Scanner(colunas_e_Linhas);
            setColunas(input.nextInt());
            setLinhas(input2.nextInt());
            pixels = lerArquivo.readLine();
            setPixel(Integer.parseInt(pixels));
            setTudoJunto(getNumeroMagico() + "\n" + getSegundaLinha() + "\n" + colunas_e_Linhas + "\n" + pixels + "\n");
            //String imagem;
            //FileWriter arq = new FileWriter("saida.txt");
            //PrintWriter gravarArq = new PrintWriter(arq);
            //imagem = lerArquivo.readLine();
            int contador=0;
            byte [] imagemNova = new byte[getColunas()*getLinhas()];
            byte [] imagemNegativa = new byte[getColunas()*getLinhas()];
            FileInputStream imagemPGM = new  FileInputStream(nomeDoArquivo);
            //FileOutputStream arq = new FileOutputStream("saida.pgm");
            //arq.write(getNumeroMagico());
            imagemPGM.skip(getTudoJunto().length());
            while(contador != (getColunas()*getLinhas())){
                imagemNova[contador] = (byte) imagemPGM.read();
                imagemNegativa[contador] = (byte) ( (byte) getPixel() - (byte) imagemNova[contador]);
                contador++;
            }
            //arq.write(getTudoJunto().getBytes());
           // arq.write(getImagemCopia());
           // System.out.print(tudoJunto);
           char[] auxiliar = new char[getTudoJunto().length()];
           for(int k = 0; k<getTudoJunto().length(); k++)  
           auxiliar[k] = getTudoJunto().toCharArray()[k];

           
           byte[] imagemCompleta = new byte[getTudoJunto().length()+getColunas()*getLinhas()];
           int i=0;
           for(int k = 0; k<getTudoJunto().length(); k++){
               imagemCompleta[k] = (byte) auxiliar[k];
               i++;
           }  
           for(int k=i ; k<getColunas()*getLinhas(); k++){
               imagemCompleta[k] = imagemNova[k];
           }
            setImagemCopia(imagemCompleta);
            
           
           byte[] imagemCompletaNegativa = new byte[getTudoJunto().length()+getColunas()*getLinhas()];
           i=0;
           for(int k = 0; k<getTudoJunto().length(); k++){
               imagemCompletaNegativa[k] = (byte) auxiliar[k];
               i++;
           }  
           for(int k=i ; k<getColunas()*getLinhas(); k++){
               imagemCompletaNegativa[k] = imagemNegativa[k];
           }
            
            setImagemNegativa(imagemCompletaNegativa);
            
            //BufferedImage plane = new BufferedImage(getColunas(), getLinhas(), BufferedImage.TYPE_BYTE_GRAY);
            //plane.getRaster().getDataBuffer().get
           
           //FileOutputStream arq = new FileOutputStream("saida.pgm");
            //arq.write(getImagemNegativa());
            
    //System.out.print(getImagemCopia());
    //gravarArq.print(getImagemCopia());
    }
    catch(IOException e){
        System.err.printf("Erro na abertura do arquivo: %s.\n", e.getMessage());
    }
    
  }
}

